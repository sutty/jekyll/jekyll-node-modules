# jekyll-node-modules

This plugin adds a Liquid tag to include files from the `node_modules`
directory.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jekyll-node-modules'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-node-modules

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- jekyll-node-modules
```

Then, on your JS files:

```yaml
---
# The front matter makes Jekyll process this file with Liquid
---

{% node_module path/inside/node_modules/file.js %}
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-node-modules>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-node-modules project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
