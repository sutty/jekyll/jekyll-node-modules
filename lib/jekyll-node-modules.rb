# Crea un tag de Liquid que busca un archivo dentro del directorio
# node_modules y lo incorpora tal cual.
module Jekyll
  class NodeModulesTag < Liquid::Tag
    def initialize(tag, markup, tokens)
      super

      @file = File.join('node_modules', markup.strip)
    end

    def render(context)
      File.read @file
    end
  end
end

# Registrar el tag
Liquid::Template.register_tag('node_module', Jekyll::NodeModulesTag)
